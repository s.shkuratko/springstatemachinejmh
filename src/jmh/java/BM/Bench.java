package BM;

import com.test.testspringtestmachine.TestSpringStateMachineApplication;
import com.test.testspringtestmachine.persister.InMemorySmPersist;
import com.test.testspringtestmachine.statemachine.prototype.PrototypeStateMachineService;
import com.test.testspringtestmachine.statemachine.singleton.SingletonStateMachineService;
import org.openjdk.jmh.annotations.*;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.ThreadLocalRandom;

@State(Scope.Benchmark)
@Fork(value = 0, warmups = 1)
@Measurement(iterations = 1)
@Warmup(iterations = 1)
@Threads(4)
@Timeout(time = 20)
public class Bench {

    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    public void testPrototype() {
        int value = ThreadLocalRandom.current().nextInt(3);
        prototypeSM.step0(value);
        prototypeSM.step1(value);
    }

    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    public void testSingleton() {
        int value = ThreadLocalRandom.current().nextInt(3);
        singletonSM.step0(value);
        singletonSM.step1(value);
    }

    SingletonStateMachineService singletonSM;
    PrototypeStateMachineService prototypeSM;
    InMemorySmPersist persist;

    @Setup(Level.Trial)
    public void init() {
        ConfigurableApplicationContext context = SpringApplication.run(TestSpringStateMachineApplication.class);
        singletonSM = context.getBean(SingletonStateMachineService.class);
        prototypeSM = context.getBean(PrototypeStateMachineService.class);
        persist = context.getBean(InMemorySmPersist.class);
    }

    @TearDown(Level.Trial)
    public void after() {
        persist.printContext();
    }


}
