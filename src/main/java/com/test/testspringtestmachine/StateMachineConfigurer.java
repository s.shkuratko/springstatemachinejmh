package com.test.testspringtestmachine;

import com.test.testspringtestmachine.persister.InMemorySmPersist;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.persist.StateMachinePersister;

import java.util.concurrent.ThreadLocalRandom;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Configuration
public class StateMachineConfigurer {

    @Bean
    public StateMachinePersister<String, String, String> ps(InMemorySmPersist persist) {
        return new DefaultStateMachinePersister<>(persist);
    }

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    @Qualifier("prototypeSM")
    public StateMachine<String, String> prototypeStateMachine() throws Exception {
        return buildStateMachine();
    }

    @Bean
    @Qualifier("singletonSM")
    public StateMachine<String, String> singletonStateMachine() throws Exception {
        return buildStateMachine();
    }


    private StateMachine<String, String> buildStateMachine() throws Exception {
        StateMachineBuilder.Builder<String, String> builder
                = StateMachineBuilder.builder();
        builder.configureStates().withStates()
                .initial("SI")
                .state("S1")
                .state("SF");

        builder.configureTransitions()
                .withExternal()
                .source("SI").target("S1").event("E1")
                .action(ctx -> {
                    Integer int_value = ctx.getExtendedState().get("INT_VALUE", Integer.class);
                    int_value = ThreadLocalRandom.current().nextInt(100500) + int_value;
                    ctx.getExtendedState().getVariables().put("NEW_INT", int_value);

                    spin(1000);

                })
                .and().withExternal()
                .source("S1").target("SF").event("E2");

        return builder.build();
    }

    private static void spin(int milliseconds) {
        long sleepTime = milliseconds * 1000000L;
        long startTime = System.nanoTime();
        while ((System.nanoTime() - startTime) < sleepTime) {
        }
    }

}
