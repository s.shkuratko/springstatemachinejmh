package com.test.testspringtestmachine.persister;

import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class InMemorySmPersist implements StateMachinePersist<String, String, String> {

    private final Map<String, StateMachineContext<String, String>> contexts = new ConcurrentHashMap<>();

    @Override
    public void write(final StateMachineContext<String, String> context, String contextObj) {
        contexts.put(contextObj, context);
    }

    @Override
    public StateMachineContext<String, String> read(final String contextObj) {
        return contexts.get(contextObj);
    }

    public void printContext() {
        for (var entry : contexts.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
            System.out.println("****");
        }

    }
}