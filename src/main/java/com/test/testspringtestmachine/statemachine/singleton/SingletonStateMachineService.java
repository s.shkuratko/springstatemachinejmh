package com.test.testspringtestmachine.statemachine.singleton;

import com.test.testspringtestmachine.statemachine.AbstractStateMachineService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class SingletonStateMachineService extends AbstractStateMachineService {

    @Autowired
    @Qualifier("singletonSM")
    private StateMachine<String, String> stateMachineSingleton;

    @Override
    public synchronized void step0(int value) {
        super.step0(value);
    }

    @Override
    public synchronized void step1(int value) {
        super.step1(value);
    }

    @Override
    public synchronized StateMachine<String, String> provideStateMachine() {
        return stateMachineSingleton;
    }

    @PostConstruct
    @SneakyThrows
    public void setup() {
        stateMachineSingleton.start();
    }
}
