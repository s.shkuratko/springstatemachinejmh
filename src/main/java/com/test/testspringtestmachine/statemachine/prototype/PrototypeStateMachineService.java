package com.test.testspringtestmachine.statemachine.prototype;

import com.test.testspringtestmachine.statemachine.AbstractStateMachineService;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

@Service
public class PrototypeStateMachineService extends AbstractStateMachineService {

    @Autowired
    @Qualifier("prototypeSM")
    private ObjectFactory<StateMachine<String, String>> factory;

    @Override
    public StateMachine<String, String> provideStateMachine() {
        StateMachine<String, String> stateMachine = factory.getObject();
        stateMachine.start();
        return stateMachine;
    }


}
