package com.test.testspringtestmachine.statemachine;

import com.test.testspringtestmachine.statemachine.singleton.SingletonStateMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.persist.StateMachinePersister;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.DefaultStateMachineContext;

@SuppressWarnings("CatchMayIgnoreException")
public abstract class AbstractStateMachineService {

    @Autowired
    private StateMachinePersister<String, String, String> persister;

    public void step0(int value) {
        try {
//            long start = System.currentTimeMillis();
//            System.out.println("in " + Thread.currentThread().getName());
            StateMachine<String, String> stateMachine = provideStateMachine();

            if (this instanceof SingletonStateMachineService) {
                rehydrateState(stateMachine);
            }

            stateMachine.getExtendedState().getVariables().put("INT_VALUE", value);
            stateMachine.sendEvent("E1");

            persister.persist(stateMachine, String.valueOf(value));

//            long end = System.currentTimeMillis();
//            long time = end - start;
//            System.out.println("out " + Thread.currentThread().getName() + " " + time + " ms waste");
        } catch (Exception e) {
        }
    }

    synchronized void rehydrateState(StateMachine<String, String> stateMachine) {
        stateMachine.getStateMachineAccessor().doWithAllRegions(sma ->
                sma.resetStateMachine(new DefaultStateMachineContext<>("SI", "E1", null, null)));
    }

    public void step1(int value) {
        try {
            StateMachine<String, String> stateMachine = provideStateMachine();
            StateMachine<String, String> restoredSM = persister.restore(stateMachine, String.valueOf(value));
            restoredSM.sendEvent("E2");
            State<String, String> state = restoredSM.getState();
            persister.persist(stateMachine, String.valueOf(value));
        } catch (Exception e) {
        }
    }

    protected abstract StateMachine<String, String> provideStateMachine() throws Exception;
}
